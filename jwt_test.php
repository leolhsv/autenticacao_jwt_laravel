<?php
/**
 * Created by PhpStorm.
 * User: Leo
 * Date: 16/08/2019
 * Time: 16:12
 */

//header.payload.signature


/***********************************************
 *                       HEADER
 ************************************************/
$header = [
    'alg' => 'HS256', //HMAC - SHA256
    'typ' => 'JWT',
];

$header_json = json_encode($header); //primeiro converte para json
$header_base64 = base64_encode($header_json); //e depois para base64
echo "Cabecalho JSON: $header_json";
echo "\n";
echo "Cabecalho JWT: $header_base64";

echo "\n\n";
/***********************************************
 *                       PAYLOAD
 ************************************************/

$payload = [
    'first_name' => 'Leonardo',
    'last_name'  => 'Vales',
    'email'      => 'leohenrique.vales@gmail.com',
    'exp'        => (new \DateTime())->getTimestamp(),
];

$payload_json = json_encode($payload); //primeiro converte para json
$payload_base64 = base64_encode($payload_json); //e depois para base64
echo "Payload JSON: $payload_json";
echo "\n";
echo "Payload JWT: $payload_base64";
echo "\n\n";


$key = '6546gfg6515865gvfgr865r';
$signature = hash_hmac('sha256', "$header_base64.$payload_base64", $key, true);
$signature_base64 = base64_encode($signature);
echo "Signatura RAW $signature";
echo "\n";
echo "Signatura JWT: $signature_base64";

$token = "$header_base64.$payload_base64.$signature_base64";

echo "\n\n";
echo "TOKEN: $token";